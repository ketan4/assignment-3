var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var SomeModelSchema = new Schema({
  name: String,
  password: String,
  dob: String,
  gender: String,
  organization: String,
});

var SomeModel = mongoose.model('SomeModel', SomeModelSchema );
module.exports = SomeModel;