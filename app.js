const express = require('express');
const MongoClient = require('mongoose');
const bodyParser = require('body-parser');
var Somemodel=require("./activity")

const app = express();

app.use(bodyParser.urlencoded({extended: true}))

MongoClient.connect('mongodb://127.0.0.1/my_database', { useNewUrlParser: true ,
useUnifiedTopology: true});

var db = MongoClient.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));


app.get('/', function(req, res) {
    res.sendFile(__dirname + '/main.html')
  })

app.post('/register',function(req,res){

    Somemodel.create({
        name:req.body.name,
        password:req.body.password,
        dob:req.body.dob,
        gender:req.body.gender,
        organization:req.body.organization
    }).then(activity=>{
        res.json(activity)
    });
});

app.delete('/register/:id', (req, res, next) => {
    Somemodel.deleteOne({_id: req.params.id}).then(
      () => {
        res.status(200).json({
          message: 'Deleted!'
        });
      }
    ).catch(
      (error) => {
        res.status(400).json({
          error: error
        });
      }
    );
  });

app.listen(3000, function() {
    console.log('listening on 3000')
  })